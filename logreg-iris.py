#! /usr/bin/env python3

import argparse
ap = argparse.ArgumentParser()
args = ap.parse_args()

import numpy as np
from sklearn.datasets import load_iris
X, y = load_iris(return_X_y=True)
print(X.shape, y.shape)
               
## Make a logit
from sklearn.linear_model import LogisticRegression
#clf = LogisticRegression(random_state=0).fit(X, y.flatten())
lr = LogisticRegression(C=1, solver="saga", max_iter=100000,
                        class_weight={0:1e0, 1:1e-1})
clf = lr.fit(X, y.flatten())
#print("Success!!")

## Print params
print()
print(clf.get_params())
print(clf.n_iter_)
print(clf.coef_.shape, clf.coef_)

## Test the logistic probabilities on the test dataset
bkgtestdata = X[(y != 0.0)]
sigtestdata = X[(y == 0.0)]
ptestbkg = clf.predict_proba(bkgtestdata)
ptestsig = clf.predict_proba(sigtestdata)
print("", "p(sig|bkg):", ptestbkg[:5,1], sep="\n")
print("", "p(sig|sig):", ptestsig[:5,1], sep="\n")

## Make logistic measure
ybkg = (clf.coef_ @ bkgtestdata.T).T
ysig = (clf.coef_ @ sigtestdata.T).T
print(ybkg.shape, ysig.shape)

def plt_hist(data, **kwargs):
    plt.hist(data, range=np.quantile(data, [0.01, 0.99]), **kwargs)

## Plot outputs
from matplotlib import pyplot as plt
nbins = 20
plt.figure()
plt.subplot(2,2,1)
#plt.hist(ptestbkg[:,0], bins=nbins, alpha=0.5)
#plt.hist(ptestbkg[:,1], bins=nbins, alpha=0.5)
plt_hist(ptestbkg[:,1], bins=nbins, alpha=0.5)
plt_hist(ptestsig[:,1], bins=nbins, alpha=0.5)

plt.subplot(2,2,2)
#plt.hist(ptestsig[:,0], bins=nbins, alpha=0.5)
#plt.hist(ptestsig[:,1], bins=nbins, alpha=0.5)
plt_hist(bkgtestdata[:,1], bins=nbins, alpha=0.5)
plt_hist(sigtestdata[:,1], bins=nbins, alpha=0.5)

plt.subplot(2,2,3)
plt_hist(bkgtestdata[:,2], bins=nbins, alpha=0.5)
plt_hist(sigtestdata[:,2], bins=nbins, alpha=0.5)

plt.subplot(2,2,4)
plt_hist(ybkg, bins=nbins, alpha=0.5)
plt_hist(ysig, bins=nbins, alpha=0.5)
plt.savefig("logreg.pdf", dpi=120)
#plt.show()
