#! /usr/bin/env python3

import argparse
ap = argparse.ArgumentParser()
ap.add_argument("BKGH5")
ap.add_argument("SIGH5")
ap.add_argument("-n", "--nsamp", dest="NSAMP", default=None, type=int, help="number of events to use in the fit")
args = ap.parse_args()

def get_features(arr):
    # jetpt,jetmass,jeteta,jetwidth,efp3,efp6,efp7,efp13,efp14,efp17,efp18,efp25,...,efp40,tau21,tau32,lha,c2,d2
    return np.array(arr)  ##< all

## Read in data from H5
import numpy as np
import h5py
fbkg = h5py.File(args.BKGH5, "r")
bkgdata = get_features(fbkg["dataset"])
bkgdata[:,1] -= 1
# bkgdata = np.genfromtxt(args.BKGCSV, delimiter=", ", skip_header=1)
print("Loaded background from", args.BKGH5)
#
fsig = h5py.File(args.SIGH5, "r")
sigdata = get_features(fsig["dataset"])
# sigdata = np.genfromtxt(args.SIGCSV, delimiter=", ", skip_header=1)
print("Loaded signal from", args.SIGH5)
#
assert bkgdata.shape[1] == sigdata.shape[1]
print(bkgdata.shape, sigdata.shape)

## Chop for fitting
bkgfitdata = bkgdata
sigfitdata = sigdata
bkgtestdata = bkgdata
sigtestdata = sigdata
if args.NSAMP:
    bkgfitdata = bkgdata[:args.NSAMP//2] 
    sigfitdata = sigdata[:args.NSAMP//2]
    bkgtestdata = bkgdata[-args.NSAMP//2:] 
    sigtestdata = sigdata[-args.NSAMP//2:]     
print(bkgfitdata.shape, sigfitdata.shape)
print(bkgtestdata.shape, sigtestdata.shape)



## Make combined feature & label fitting datasets
X = np.vstack( (bkgfitdata[:,4:-5], sigfitdata[:,4:-5]) )
y = np.vstack( ( (bkgfitdata[:,1]/bkgfitdata[:,0]).reshape(-1,1), (sigfitdata[:,1]/sigfitdata[:,0]).reshape(-1,1)) )
print(X.shape, y.shape)

## Fit EFP linear combn to a classic observable
from sklearn.linear_model import LinearRegression, Ridge, Lasso
#reg = LinearRegression().fit(X, y)
reg = Ridge().fit(X, y)

## Print params
print()
Xtest = np.vstack( (bkgtestdata[:,4:-5], sigtestdata[:,4:-5]) )
ytest = np.vstack( ( (bkgtestdata[:,1]/bkgtestdata[:,0]).reshape(-1,1), (sigtestdata[:,1]/sigtestdata[:,0]).reshape(-1,1)) )
print(reg.score(Xtest, ytest))
print(reg.coef_.shape, reg.coef_)


# ## Test the logistic probabilities on the test dataset
# ptestbkg = clf.predict_proba(bkgtestdata)
# ptestsig = clf.predict_proba(sigtestdata)
# print("", "p(sig|bkg):", ptestbkg[:5,1], sep="\n")
# print("", "p(sig|sig):", ptestsig[:5,1], sep="\n")

# ## Make logistic measure
# ybkg = (clf.coef_ @ bkgtestdata.T).T
# ysig = (clf.coef_ @ sigtestdata.T).T
# print(ybkg.shape, ysig.shape)

# def plt_hist(data, **kwargs):
#     plt.hist(data, range=np.quantile(data, [0.01, 0.99]), **kwargs)

# ## Plot outputs
# from matplotlib import pyplot as plt
# plt.figure()
# plt.subplot(2,2,1)
# #plt.hist(ptestbkg[:,0], bins=100, alpha=0.5)
# #plt.hist(ptestbkg[:,1], bins=100, alpha=0.5)
# plt_hist(ptestbkg[:,1], bins=100, alpha=0.5)
# plt_hist(ptestsig[:,1], bins=100, alpha=0.5)

# plt.subplot(2,2,2)
# #plt.hist(ptestsig[:,0], bins=100, alpha=0.5)
# #plt.hist(ptestsig[:,1], bins=100, alpha=0.5)
# plt_hist(bkgtestdata[:,1], bins=100, alpha=0.5)
# plt_hist(sigtestdata[:,1], bins=100, alpha=0.5)

# plt.subplot(2,2,3)
# plt_hist(bkgtestdata[:,2], bins=100, alpha=0.5)
# plt_hist(sigtestdata[:,2], bins=100, alpha=0.5)

# plt.subplot(2,2,4)
# plt_hist(ybkg, bins=100, alpha=0.5)
# plt_hist(ysig, bins=100, alpha=0.5)
# plt.savefig("logreg.pdf", dpi=120)
# #plt.show()
