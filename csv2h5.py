#! /usr/bin/env python

import argparse
ap = argparse.ArgumentParser()
ap.add_argument("FCSV")
ap.add_argument("FH5", nargs="?", default=None)
args = ap.parse_args()
if not args.FH5:
    args.FH5 = args.FCSV[:args.FCSV.find(".csv")]+".h5"
    print(f"Writing output to {args.FH5}")

import numpy as np
data = np.genfromtxt(args.FCSV, delimiter=", ", skip_header=1)
# TODO: read header and pass through as structured-array column headings
print(f"Read {data.shape} dataset from {args.FH5}")

import h5py
with h5py.File(args.FH5, "w") as f:
    # TODO: allow control of name, type, compression, chunks, etc.
    dset = f.create_dataset("dataset", data.shape, dtype="f", chunks=True, compression="gzip", maxshape=(None,data.shape[1]))
    dset[...] = data
