#! /usr/bin/env python3

nstep = 5
nsmin = 10
nbmin = 10

def getefps(arr):
    # jetpt,jetmass,jeteta,jetwidth,efp3,efp6,efp7,efp13,efp14,efp17,efp18,efp25,...,efp40,tau21,tau32,lha,c2,d2
    return arr[:,4:-5]

def lcfn(arr, coeffs):
    subarr = getefps(arr)
    coearr = np.hstack((coeffs, [1-sum(coeffs)]))
    assert subarr.shape[1] == coearr.size
    resarr = subarr * coearr
    #print(resarr)
    rtnarr = np.sum(resarr, axis=1)
    return rtnarr
#print(lcfn(sigdata, [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7])[:10])

# TODO: s and b xsec scaling
def llr(ss, bs):
    "Poisson log likelihood ratio between s+b and b only, assuming b observed" #< ???
    assert ss.shape == bs.shape
    ks = bs
    sbs = ss+bs
    llrs = ks*(np.log(sbs) - np.log(bs)) - (sbs-bs)
    return sum(llrs)

def lcbins(sig, bkg, coeffs, sigscale, bkgyield):
    "Try increasingly finely binned uniform histograms until there's an important empty-bkg bin, then return the LLR"
    lcs = lcfn(sig, coeffs)
    lcb = lcfn(bkg, coeffs)
    nbins = 0
    edges = None
    hlcs, hlcb = None, None
    hlcs_filt, hlcb_filt = None, None
    n_lcs_old, n_lcb_old = lcs.shape[0], lcb.shape[0]
    lcs = lcs[lcs > 0] #< filter out -1 entries (what from?)
    lcb = lcb[lcb > 0] #< filter out -1 entries (what from?)
    n_lcs_new, n_lcb_new = lcs.shape[0], lcb.shape[0]
    # Check filtering efficiencies -> 99.9%, no bias
    #print("#S, #B: ", n_lcs_old, "->", n_lcs_new, "({.2f}),".format(n_lcs_new/n_lcs_old), n_lcb_old, "->", n_lcb_new, "({.2f})".format(n_lcb_new/n_lcb_old))

    while nbins <= args.NBINS_MAX: # find the finest binning with each non-zero signal partnered with a non-zero background
        if args.NBINS_FIX:
            nbins_next = args.NBINS_FIX
        else:
            nbins_next = (nbins+nstep) if nbins else 1
        hlcs_next, edges_next = np.histogram(np.log(lcs), nbins_next)
        hlcb_next, _ = np.histogram(np.log(lcb), edges_next)
        assert hlcs_next.shape == hlcb_next.shape
        hlcs_next_filt = hlcs_next[(hlcs_next >= nsmin)]
        hlcb_next_filt = hlcb_next[(hlcs_next >= nsmin)]
        assert hlcs_next_filt.shape == hlcb_next_filt.shape
        #print("?", nbins_next, hlcs_next_filt, hlcb_next_filt)
        if not np.all(hlcb_next_filt > nbmin): # some empty bkg bins where sig > 0
            break
        edges = edges_next
        hlcs, hlcb = hlcs_next, hlcb_next
        hlcs_filt, hlcb_filt = hlcs_next_filt, hlcb_next_filt
        nbins = nbins_next
        if args.NBINS_FIX:
            break
    print("*", coeffs, nbins, hlcs_filt, hlcb_filt, hlcs_filt/hlcb_filt, sep="\n")

    # from matplotlib import pyplot as plt
    # plt.figure()
    # plt.plot(edges[:-1], hlcb)
    # plt.plot(edges[:-1], hlcs)
    # plt.yscale("log")
    # plt.show()

    ## Yield-scaling
    sf_s = sigscale * bkgyield / np.sum(hlcs)
    sf_b = bkgyield / np.sum(hlcb)
    hlcs_scaled = sf_s * hlcs
    hlcb_scaled = sf_b * hlcb
    hlcs_filt_scaled = sf_s * hlcs_filt
    hlcb_filt_scaled = sf_b * hlcb_filt
    return hlcs_filt_scaled, hlcb_filt_scaled, hlcs_scaled, hlcb_scaled, edges

def lcllr(sig, bkg, coeffs, sigscale, bkgyield):
    """\
    Calculate the LLR for the given linear combination and yield-scaling
    """
    hlcs_filt, hlcb_filt, hlcs, hlcb, edges = lcbins(sig, bkg, coeffs, sigscale, bkgyield)
    # sf_s = sigscale * bkgyield / np.sum(hlcs)
    # sf_b = bkgyield / np.sum(hlcb)
    # hlcs_filt_scaled = sf_s * hlcs_filt
    # hlcb_filt_scaled = sf_b * hlcb_filt
    hlcs_filt_scaled = hlcs_filt
    hlcb_filt_scaled = hlcb_filt
    return llr(hlcs_filt_scaled, hlcb_filt_scaled)


import argparse
ap = argparse.ArgumentParser()
ap.add_argument("BKGH5")
ap.add_argument("SIGH5")
ap.add_argument("--nbins", dest="NBINS_FIX", metavar="N", type=int, default=None, help="fix the number of bins used")
ap.add_argument("--nbins-max", dest="NBINS_MAX", metavar="N", type=int, default=50, help="the max number of dynamic bins allowed")
args = ap.parse_args()

import numpy as np
import h5py
fbkg = h5py.File(args.BKGH5, "r")
bkgdata = fbkg["dataset"]
# bkgdata = np.genfromtxt(args.BKGCSV, delimiter=", ", skip_header=1)
print("Loaded background from", args.BKGH5)
fsig = h5py.File(args.SIGH5, "r")
sigdata = fsig["dataset"]
# sigdata = np.genfromtxt(args.SIGCSV, delimiter=", ", skip_header=1)
print("Loaded signal from", args.SIGH5)
assert bkgdata.shape[1] == sigdata.shape[1]
#print(bkgdata.shape, sigdata.shape)

def lcmetric(coeffs):
    #rtn = np.std( lcfn(sigdata, coeffs) )
    rtn = lcllr(sigdata, bkgdata, coeffs, SIGSCALE, BKGYIELD)
    FUNVALS.append(rtn)
    print(rtn, "\n")
    return rtn
#print(lcmetric(sigdata, [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7]))

import scipy as sp
FUNVALS = []
# TODO: include trigger prescale factor? and MC stat error... this is the same for all scalings: hmm
SIGSCALE = 1e-1
BKGYIELD = 1e-10*3000*1e9 # xsec in fb...
HLCS, HLCB, EDGES = None, None, None

## Choose the starting params so each EFP contributes the same on average
# params0 = [1/8 for i in range(7)]
# params0 = [0.1, 0.3, 0.05, -0.2, 0.2, -0.1, -0.35]
meanvals = np.mean(getefps(sigdata), axis=0)
params0 = 1 / meanvals[:-1] / meanvals.size

## Optimise the coeffs of the linear combination to maximise expected LLR
opt = sp.optimize.minimize(lcmetric, params0, jac="3-point", method="SLSQP")
#options={"ftol":0.05, "gtol":0.01}) # return_all=True)
print(opt)
print(opt.fun)

from matplotlib import pyplot as plt
plt.figure()
plt.plot(np.abs(FUNVALS))
plt.yscale("log")
plt.savefig("lcllropt.pdf")
plt.show()

plt.figure()
hlcs_filt, hlcb_filt, hlcs, hlcb, edges = lcbins(sigdata, bkgdata, opt.x, SIGSCALE, BKGYIELD)
plt.plot(edges[:-1], hlcb)
plt.plot(edges[:-1], hlcs)
plt.plot(edges[:-1], hlcb+hlcs)
plt.yscale("log")
plt.savefig("lcobs.pdf")
plt.show()

fbkg.close()
fsig.close()
